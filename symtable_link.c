#include<conio.h>
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<string.h>
#include<symtable.h>
struct symtable {
   char *key;
   void * value;
   struct symtable * next;
};
typedef struct symtable* SymTable_t;

SymTable_t SymTable_new(void)
{
	SymTable_t current;
	current = (SymTable_t)malloc(sizeof(SymTable_t));
	current->key = NULL;
	current->next = NULL;
	current->value = NULL;
    if(current==NULL)
    {
    return NULL;
	}
    else
    {
    return current ;
	}
}
void SymTable_free(SymTable_t oSymTable)
{
	
	if (oSymTable == NULL)
      return;
	free(oSymTable);
}
int SymTable_getLength (SymTable_t oSymTable)
{

assert(oSymTable!= NULL);
int length = 0;
        while(oSymTable !=NULL )
        {
           length++;
           oSymTable =oSymTable->next;
        }
       return length; 	
}
char *SymTable_get (SymTable_t oSymTable, const char *pcKey)
{
	if(oSymTable == NULL)
		return NULL;
	else
	{
		if(strcmp(oSymTable->key,pcKey))
		{
			return oSymTable->value;
		}
		while(oSymTable->next != NULL)
		{
			if(strcmp(oSymTable->key,pcKey))
			{
	   			return oSymTable->value;
			}
			oSymTable= oSymTable->next;
		}
		return NULL;
	}
  
}
int SymTable_contains (SymTable_t oSymTable, const char *pcKey)
{

     if(strcmp(oSymTable->key,pcKey)==0)
     return 1;
     else 
     return 0;

}
